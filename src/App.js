import React from 'react';
import Policy from './pages/Policy';
import Landing from './pages/Landing';
import Organization from './pages/Organization';
import PolicyCreate from './pages/PolicyCreate';
import { Route, Switch, Redirect } from 'react-router-dom';
import OrganizationCreate from './pages/OrganizationCreate';

import ModalManager from './ui/Modal/ModalManager.component';

class App extends React.Component {
  render() {
    return (
      <React.Fragment>

        <ModalManager />

        <Switch>
          <Route path="/" exact component={Landing} />
          <Route path="/policy" exact component={Policy} />
          <Route path="/organization" exact component={Organization} />
          <Route path="/policy-create/:policyId" exact component={PolicyCreate} />
          <Route path="/organization-create/:organizationSlug" exact component={OrganizationCreate} />
          <Redirect to="/" />
        </Switch>

      </React.Fragment>
    );
  }
}

export default App;