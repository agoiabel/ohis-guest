import React from 'react';
import {connect} from 'react-redux';
import Header from '../../ui/Header';
import Footer from '../../ui/Footer';
import PolicyCard from '../../ui/Policy';
import Category from '../../ui/Category';
import styles from './Landing.page.module.css';
import CustomButton from '../../ui/CustomButton';
import SearchForm from '../../ui/forms/SearchForm';

import { get_ohis_default_data } from '../../shared/store/root.action.js';

class Landing extends React.Component {

	componentDidMount() {
		this.props.get_ohis_default_data();
	}

	componentWillReceiveProps(nextProps) {
		console.dir(nextProps);
	}

	render() {

		let policies;


		if (this.props.status === 200) {
			policies = this.props.policies.map(policy => (
				<PolicyCard
					name={policy.name}
					amount={policy.price.toLocaleString()}
					services={[
						{
							name: `${policy.name}`
						},
						{
							name: `Maximum ${policy.maximum_no_of_beneficiary_dependant} Dependents`
						},
						// {
						// 	name: policy.name
						// }
					]}
					policyId={policy.id}
					key={policy.id}
				/>
			));
		}


		return (
			<div>

				<Header />


				<div className={styles.siteIntro}>
					<div className={styles.container}>
						<div className={styles.siteIntroContainer}>
							<div className={styles.siteIntroDescription}>
								<div className={styles.siteIntroHeader}>Osun Health Solutions.</div>
								<div className={styles.description}>
									Whether you are a health care provider, a health maintenance organization (HMO),
									a company or an individual, Health Connect provides a platform for everything you
									need to offer health care services, purchase and manage health insurance policies.
								</div>

								<div className={styles.searchForm}>
									<SearchForm />
								</div>
							</div>
							<div className={styles.introImage}><img src={require('../../assets/images/introDesciption.png')} /></div>
						</div>
					</div>
				</div>


				<div className={styles.categoryContainer}>
					<div className={styles.container}>

						<div className={styles.containerHeaderContainer}>
							<div className={styles.containerHeaderTitle}>A Single Platform for All</div>
							<div className={styles.containerHeaderSubTitle}>Select a category below to get started</div>
						</div>

						<div className={styles.categories}>

							<Category
								name="CORPORATE/COMPANY"
								description="Enroll your organization for the State Insurance Coverage for your Employees."
								url="/organization-create/new-company"
							/>

							<Category
								name="PROVIDER"
								description="Enroll your organization for the State Insurance Scheme as Healthcare Provider."
								url="/organization-create/new-hospital"
							/>

							<Category
								name="BROKER"
								description="Enroll your organization for the State Insurance Scheme as an Intermediary to Enrollees."
								url="new-hmo"
							/>

							<Category
								name="INDIVIDUAL"
								description="Enroll yourself/family members for the State Insurance Coverage."
								url="/policy"
							/>

						</div>
					</div>
				</div>


				<div className={styles.governorContainer}>
					<div className={styles.container}>
						<div className={styles.governorImageSpeech}>
							<div className={styles.governorImage}><img src={require('../../assets/images/oyetola.png')} /></div>
							<div className={styles.governorSpeech}>
								<div>
									"Revitalizing Primary Healthcare Centres PHCs; thereby giving access to all citizens whether you reside in the inner most part of our rural areas or semi urban centres and this will in turn reduce child and maternal mortality, communicable and non-communicable diseases which will give credence to a more healthier and productive citizenry".
								</div>
								<div className={styles.governorTitle}>
									<div className={styles.governorName}>Gboyega Oyetola</div>
									<div className={styles.governorOffice}>Executive Governor of Osun State</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div className={styles.policyContainer}>
					<div className={styles.container}>

						<div className={styles.policyHeader}>
							<div className={styles.policyHeaderTitle}>OHIS Policies</div>
							<div className={styles.policyHeaderSubTitle}>Select from one our OHIS policies</div>
						</div>

						<div className={styles.policies}>

							{policies}

						</div>



						{/* <div className={styles.viewMorePolicies}>
							<CustomButton>View more policies</CustomButton>
						</div> */}

					</div>
				</div>


				<Footer />
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		status: state.rootReducer.status,
		policies: state.rootReducer.policies
	}
}

const mapDispatchToProps = dispatch => {
	return {
		get_ohis_default_data: () => dispatch( get_ohis_default_data() )
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Landing);