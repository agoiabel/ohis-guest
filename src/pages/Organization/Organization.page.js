import React from 'react';
import Header from '../../ui/Header';
import Category from '../../ui/Category';
import styles from './Organization.page.module.css';

class Organization extends React.Component {
	render() {
		return (
			<div>
				<div>
					<Header />
				</div>


				<div className={styles.container}>
					<div className={styles.header}>
						Select A Category to Get Started
					</div>

					<div className={styles.categories}>
						<Category 
							showBorder  
							name="COMPANY"
							description="Enroll your organization for the State Insurance Coverage for your Employees."
							url="new-company"	
						/>

						<Category
							showBorder
							name="PROVIDER"
							description="Enroll your organization for the State Insurance Coverage for your Employees."
							url="new-hospital"
						/>

						<Category
							showBorder
							name="HMO"
							description="Enroll your organization for the State Insurance Coverage for your Employees."
							url="new-hmo"
						/>

						<Category
							showBorder
							name="COMPANY"
							description="Enroll your organization for the State Insurance Coverage for your Employees."
							url="new-company"
						/>

					</div>
				</div>


			</div>
		);
	}
}

export default Organization;