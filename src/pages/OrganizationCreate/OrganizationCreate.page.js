import React from 'react';
import Header from '../../ui/Header';
import { connect } from 'react-redux';

import SuccessCard from '../../ui/SuccessCard';
import styles from './OrganizationCreate.page.module.css';

import { local_governments } from '../../utils/local_government';

import { store_organization } from '../../shared/store/root.action';
import OrganizationStepOneForm from '../../ui/forms/OrganizationStepOneForm';
import OrganizationStepTwoForm from '../../ui/forms/OrganizationStepTwoForm';
import OrganizationStepThreeForm from '../../ui/forms/OrganizationStepThreeForm';


class OrganizationCreate extends React.Component {

    state = {
        activeStep: 1,
        steps: {
            1: {
                percentage: '33.3%',
                description: 'Organization Profile. You are required to provide accurate infomation of your organization'
            },
            2: {
                percentage: '67%',
                description: 'Organization Profile. You are required to provide accurate infomation of your organization'
            },
            3: {
                percentage: '100%',
                description: 'Organization Profile. You are required to provide accurate infomation of your organization'
            },
            4: {
                percentage: '100%',
                description: 'Organization Profile. You are required to provide accurate infomation of your organization'
            }
        },
        formData: {
            warden: ''
        },
        picture: null,
        organizationName: '',
        submittingForm: false,
        organization_type_id: null
    }


    nextStep = () => {
        this.setState({
            activeStep: this.state.activeStep + 1
        });
    }


    previousStep = () => {
        if (this.state.activeStep === 1) {
            return this.state.activeStep;
        }

        this.setState({
            activeStep: this.state.activeStep - 1
        });
    }


    checkSlug = slug => {
        if ((slug != 'new-hmo') && (slug != 'new-company') && (slug != 'new-hospital')) {
            return this.props.history.push('/');
        } 
    } 


    navigateOnSuccessWith = props => {
        if (props.store_beneficiary_status === 200) {
            this.nextStep();
        }
    }

    componentDidMount() {
        this.checkSlug(this.props.match.params.organizationSlug);
        this.getNameFrom(this.props.match.params.organizationSlug);
    }


    componentWillReceiveProps(nextProps) {
        this.navigateOnSuccessWith(nextProps);
    }


    getNameFrom = slug => {
        if (slug == 'new-hmo') {
            this.setState({
                organizationName: 'HMO',
                organization_type_id: 1,
            });
        } 
        if (slug == 'new-company') {
            this.setState({
                organizationName: 'Company',
                organization_type_id: 3,
            });
        }
        if (slug == 'new-hospital') {
            this.setState({
                organizationName: 'Provider',
                organization_type_id: 2,
            });
        } 
    }


    handleStepThree = file => {
        this.setState({
            picture: file
        }, () => {
            let form = {};
            form['is_active'] = '0';
            form['organization_type_id'] = this.state.organization_type_id;

            return this.setState({
                submittingForm: true,
                formData: { ...this.state.formData, ...form }
            }, () => {
                return this.props.store_organization({
                    data: this.state.formData,
                    picture: this.state.picture
                });
            });
        });

        return this.nextStep();
    }


    handleStepSubmit = formData => {
        this.setState({
            formData: { ...this.state.formData, ...formData }
        }, () => {
            return this.nextStep();         
        });
    }


    render() {

        let currentForm;

        let introductoryHeader = (
            <div className={styles.container}>
                <div className={styles.header}>
                    <div className={styles.title}>Register {this.state.organizationName}</div>
                    <div className={styles.subTitle}>
                        {this.state.steps[this.state.activeStep].description}
                    </div>
                </div>
            </div>
        );

        let percentageCounter = (
            <div className={styles.sliderContainer}>
                <div className={styles.slider} style={{ width: `${this.state.steps[this.state.activeStep].percentage}` }}></div>
            </div>
        );

        if (this.state.activeStep == 1) {
            currentForm = <OrganizationStepOneForm onSubmit={this.handleStepSubmit} 
                                                   local_governments={local_governments} 
                                                   type={this.props.match.params.organizationSlug} 
                        />
        }
        if (this.state.activeStep == 2) {
            currentForm = <OrganizationStepTwoForm onSubmit={this.handleStepSubmit} previousStep={this.previousStep} />
        }
        if (this.state.activeStep == 3) {
            currentForm = <OrganizationStepThreeForm onSubmit={this.handleStepThree} previousStep={this.previousStep} submittingForm={this.state.submittingForm} />
        }
        if (this.state.activeStep == 4) {
            introductoryHeader = null;
            percentageCounter = null;
            currentForm = <SuccessCard />
        }


        return (
            <div>
                <div>
                    <Header />
                </div>

                { introductoryHeader }

                <div className={styles.container}>

                    { percentageCounter }


                    <div className={styles.formContainer}>
                        
                        { currentForm }

                    </div>

                </div>

            </div>
        );
    }

}


const mapStateToProps = state => {
    return {
        states: state.rootReducer.states,
        store_organization_status: state.rootReducer.store_organization_status
    }
}

const mapDispatchToProps = dispatch => {
    return {
        store_organization: payload => dispatch(store_organization(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrganizationCreate);