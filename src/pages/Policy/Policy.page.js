import React from 'react';
import { connect } from 'react-redux';
import Header from '../../ui/Header';
import PolicyCard from '../../ui/Policy';
import styles from './Policy.page.module.css';
import { get_ohis_default_data } from '../../shared/store/root.action.js';

class Policy extends React.Component {


	componentDidMount() {
		this.props.get_ohis_default_data();
	}



	render() {

		let policies;


		if (this.props.defaultHmo !== null) {
			policies = this.props.defaultHmo.hmo_policy_sets.map(hmo_policy_set => {
				return hmo_policy_set.policies.map(policy => (
					<PolicyCard
						showBorder
						name={policy.name}
						amount={`# ${policy.price}`}
						services={[
							{
								name: `${policy.policy_set.name}`
							},
							{
								name: `${policy.maximum_no_of_beneficiary_dependant} Dependents`
							},
							{
								name: policy.name
							}
						]}
						policyId={policy.id}
						key={policy.id}
					/>
				));

			});
		}


		return (
			<div>
				<div>
					<Header />
				</div>


				<div className={styles.container}>
					<div className={styles.header}>
						Select A Policy to Get Started
					</div>

					<div className={styles.policies}>

						{policies}

					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		defaultHmo: state.rootReducer.defaultHmo
	}
}

const mapDispatchToProps = dispatch => {
	return {
		get_ohis_default_data: () => dispatch(get_ohis_default_data())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Policy);