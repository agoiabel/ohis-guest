import React from 'react';
import {connect} from 'react-redux';
import Header from '../../ui/Header';
import styles from './PolicyCreate.page.module.css';

import SuccessCard from '../../ui/SuccessCard';
import { local_governments } from '../../utils/local_government';

import BeneficiaryPlanDetail from '../../ui/BeneficiaryPlanDetail';
import BeneficiaryStepSixForm from '../../ui/forms/BeneficiaryStepSixForm';
import BeneficiaryStepOneForm from '../../ui/forms/BeneficiaryStepOneForm';
import BeneficiaryStepTwoForm from '../../ui/forms/BeneficiaryStepTwoForm';
import BeneficiaryStepFourForm from '../../ui/forms/BeneficiaryStepFourForm';
import BeneficiaryStepFiveForm from '../../ui/forms/BeneficiaryStepFiveForm';
import BeneficiaryStepThreeForm from '../../ui/forms/BeneficiaryStepThreeForm';


import BeneficiaryCard from '../../ui/BeneficiaryCard';

import { store_beneficiary, get_ohis_default_data } from '../../shared/store/root.action';


class PolicyCreate extends React.Component {

    state = {
        activeStep: 0,
        steps: {
            0: {
                percentage: '12.5%',
                description: 'Personal Info. You are required to provide accurate personal infomation and that of Next of Kin'
            },
            1: {
                percentage: '25%',
                description: 'Personal Info. You are required to provide accurate personal infomation and that of Next of Kin'
            },
            2: {
                percentage: '37.5%',
                description: 'Personal Info. You are required to provide accurate personal infomation and that of Next of Kin'
            },
            3: {
                percentage: '50%',
                description: 'Personal Info. You are required to provide accurate personal infomation and that of Next of Kin'
            },
            4: {
                percentage: '62.5%',
                description: 'Personal Info. You are required to provide accurate personal infomation and that of Next of Kin'
            },
            5: {
                percentage: '75%',
                description: 'Personal Info. You are required to provide accurate personal infomation and that of Next of Kin'
            },
            6: {
                percentage: '87.5%',
                description: 'Personal Info. You are required to provide accurate personal infomation and that of Next of Kin'
            },
            7: {
                percentage: '100%',
                description: 'Personal Info. You are required to provide accurate personal infomation and that of Next of Kin'
            },
            8: {
                percentage: '100%',
                description: 'Personal Info. You are required to provide accurate personal infomation and that of Next of Kin'
            },
        },
        policyName: 'Policy',
        formData: {
            policy_id: this.props.match.params.policyId,
        },
        policy: null,
        submittingForm: false,
        picture: null
    }


    nextStep = () => {
        let nextStep = this.state.activeStep + 1;

        if (this.state.activeStep == 7) {
            nextStep = 0;
        }

        this.setState({
            activeStep: nextStep
        });
    }


    previousStep = () => {
        if (this.state.activeStep === 0) {
            return this.state.activeStep;
        }

        this.setState({
            activeStep: this.state.activeStep - 1
        });
    }

    start = () => {
        return this.nextStep();
    }


    handleStepSubmit = formData => {
        
        this.setState({
            formData: { ...this.state.formData, ...formData }
        }, () => {

            if (this.state.activeStep == 6) {

                let form = {};
                form['code'] = this.state.formData.primary_phone_number;

                return this.setState({
                    submittingForm: true,
                    formData: { ...this.state.formData, ...form }
                }, () => {
                    return this.props.store_beneficiary({
                        data: this.state.formData,
                        picture: this.state.picture
                    }); 
                });
            }

            return this.nextStep();
        });
    }


    handleStepTwoForm = file => {
        this.setState({
            picture: file
        });

        return this.nextStep();
    }

    getPolicyFrom = props => {
        if (props.status === 200) {

            const form = {};
            form['company_id'] = props.company.id;
            form['agent_id'] = props.company.owner_id;
            const formData = { ...this.state.formData, ...form };

            this.setState({
                formData: formData,
                policy: props.policies.find(policy => policy.id == this.props.match.params.policyId)
            });
        }        
    }

    navigateOnSuccessWith = props => {
        if (props.store_beneficiary_status === 200) {
            this.nextStep();
        }
    }

    componentDidMount() {
        this.props.get_ohis_default_data();
    }

    componentWillReceiveProps(nextProps) {
        this.getPolicyFrom(nextProps);
        this.navigateOnSuccessWith(nextProps);
    }

    componentWillUnmount() {
        this.setState({
            policy: []
        });
    }

    render() {

        let currentForm;

        let introductoryHeader = (
            <div className={styles.container}>
                <div className={styles.header}>
                    <div className={styles.title}>Register for {this.state.policyName}</div>
                    <div className={styles.subTitle}>
                        {this.state.steps[this.state.activeStep].description}
                    </div>
                </div>
            </div>
        );

        let percentageCounter = (
            <div className={styles.sliderContainer}>
                <div className={styles.slider} style={{ width: `${this.state.steps[this.state.activeStep].percentage}` }}></div>
            </div>
        );

        if (this.props.status === 200) {
            if (this.state.activeStep == 0) {
                currentForm = <BeneficiaryPlanDetail onSubmit={this.start} policy={this.state.policy} />
            }
            if (this.state.activeStep == 1) {
                currentForm = <BeneficiaryStepOneForm 
                                onSubmit={this.handleStepSubmit} 
                                previousStep={this.previousStep} 
                                providers={this.props.providers} 
                                local_governments={local_governments}    
                            />
            }
            if (this.state.activeStep == 2) {
                currentForm = <BeneficiaryStepTwoForm onSubmit={this.handleStepTwoForm} previousStep={this.previousStep} />
            }
            if (this.state.activeStep == 3) {
                currentForm = <BeneficiaryStepThreeForm onSubmit={this.handleStepSubmit} previousStep={this.previousStep} />
            }
            if (this.state.activeStep == 4) {
                currentForm = <BeneficiaryStepFourForm onSubmit={this.handleStepSubmit} previousStep={this.previousStep} />
            }
            if (this.state.activeStep == 5) {
                currentForm = <BeneficiaryStepFiveForm onSubmit={this.handleStepSubmit} previousStep={this.previousStep} />
            }
            if (this.state.activeStep == 6) {
                currentForm = <BeneficiaryStepSixForm onSubmit={this.handleStepSubmit} previousStep={this.previousStep} submittingForm={this.state.submittingForm} />
            }
            if (this.state.activeStep == 7) {
                currentForm = <BeneficiaryCard nextStep={this.nextStep} beneficiary={this.props.beneficiary} transaction={this.props.transaction} />
            }
        }


        return (
            <div>
                <div>
                    <Header />
                </div>

                { introductoryHeader }

                <div className={styles.container}>

                    { percentageCounter }


                    <div className={styles.formContainer}>
                        
                        { currentForm }

                    </div>

                </div>

            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
        status: state.rootReducer.status,
        company: state.rootReducer.company,
        policies: state.rootReducer.policies,
        providers: state.rootReducer.providers,
        beneficiary: state.rootReducer.beneficiary,
        transaction: state.rootReducer.transaction,
        store_beneficiary_status: state.rootReducer.store_beneficiary_status
    }
}

const mapDispatchToProps = dispatch => {
    return {
        store_beneficiary: payload => dispatch(store_beneficiary(payload)),
        get_ohis_default_data: () => dispatch(get_ohis_default_data())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PolicyCreate);