import {combineReducers} from 'redux';

import rootReducer from './store/root.reducer';
import { reducer as formReducer } from 'redux-form';
import modalReducer from '../ui/Modal/Modal.reducer';
import takePictureReducer from '../ui/Modal/TakePicture/TakePicture.reducer';

export default combineReducers({
    form: formReducer,
    rootReducer: rootReducer,
    modalReducer: modalReducer,
    takePictureReducer: takePictureReducer
});