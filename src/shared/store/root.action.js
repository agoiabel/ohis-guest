import { get, postWithImage } from '../../utils/http_client';
import { GET_OHIS_DEFAULT_DATA_SUCCESSFUL, GET_OHIS_DEFAULT_DATA_UNSUCCESSFUL, STORE_BENEFICIARY_WAS_SUCCESSFUL, STORE_BENEFICIARY_WAS_UNSUCCESSFUL, STORE_ORGANIZATION_WAS_SUCCESSFUL, STORE_ORGANIZATION_WAS_UNSUCCESSFUL} from './index';



export const get_ohis_default_data = () => async dispatch => {

    try {
        let response = await get('tenant/index');

        response = await response.json();

        if (response.status !== 200) {
            return window.setTimeout((() => {
                dispatch(get_ohis_default_data_unsuccessful(response));
            }));
        }

        dispatch(get_ohis_default_data_successful(response));
    } catch (error) {
        console.dir(error);
    }

};


export const get_ohis_default_data_successful = payload => {
    return {
        type: GET_OHIS_DEFAULT_DATA_SUCCESSFUL,
        payload: payload
    };
}
export const get_ohis_default_data_unsuccessful = payload => {
    return {
        type: GET_OHIS_DEFAULT_DATA_UNSUCCESSFUL,
        payload: payload
    }
}





export const store_organization = payload => async dispatch => {
    try {
        let response = await postWithImage('picture', payload, 'organization_profile/store');

        response = await response.json();

        if (response.status !== 200) {
            return window.setTimeout((() => {
                dispatch(store_organization_was_unsuccessful(response));
            }));
        }
        dispatch(store_organization_was_successful(response));
    } catch (error) {
        console.dir(error);
    }
};


export const store_organization_was_successful = payload => {
    return {
        type: STORE_ORGANIZATION_WAS_SUCCESSFUL,
        payload: payload
    };
}


export const store_organization_was_unsuccessful = payload => {
    return {
        type: STORE_ORGANIZATION_WAS_UNSUCCESSFUL,
        payload: payload
    }
}


export const store_beneficiary = payload => async dispatch => {
    try {
        let response = await postWithImage('picture', payload, 'beneficiary/store');

        response = await response.json();

        if (response.status !== 200) {
            return window.setTimeout((() => {
                dispatch(store_beneficiary_was_unsuccessful(response));
            }));
        }
        
        dispatch(store_beneficiary_was_successful(response));
    } catch (error) {
        console.dir(JSON.stringify(error));
    }
};


export const store_beneficiary_was_successful = payload => {
    return {
        type: STORE_BENEFICIARY_WAS_SUCCESSFUL,
        payload: payload
    };
}


export const store_beneficiary_was_unsuccessful = payload => {
    return {
        type: STORE_BENEFICIARY_WAS_UNSUCCESSFUL,
        payload: payload
    }
}