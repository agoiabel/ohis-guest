import { updateObject } from '../../utils/updateObject';
import { GET_OHIS_DEFAULT_DATA_SUCCESSFUL, GET_OHIS_DEFAULT_DATA_UNSUCCESSFUL, STORE_BENEFICIARY_WAS_SUCCESSFUL, STORE_BENEFICIARY_WAS_UNSUCCESSFUL, STORE_ORGANIZATION_WAS_SUCCESSFUL, STORE_ORGANIZATION_WAS_UNSUCCESSFUL } from './index';

const getTenantDefaultDataWasSuccessful = (state, action) => {
    return updateObject(state, {
        status: action.payload.status,
        company: action.payload.data.company,
        policies: action.payload.data.policies,
        providers: action.payload.data.providers,
    });
}

const getTenantDefaultDataWasUnsuccessful = (state, action) => {
    return updateObject(state, {
        get_outline_status: action.payload.status,
        get_outline_message: action.payload.message,
    });
}

const storeBeneficiaryWasSuccessful = (state, action) => {
    return updateObject(state, {
        store_beneficiary_status: 200,
        beneficiary: action.payload.data.role_user,
        transaction: action.payload.data.transaction,
    })
}

const storeOrganizationWasSuccessful = (state, action) => {
    return updateObject(state, {
        store_organization_status: 200,
    })
}

const initialState = {
    status: null,
    beneficiary: null,
    transaction: null,
    store_beneficiary_status: null,
    store_organization_status: null
};

const reducer = (state = initialState, action) => {
    const lookup = {
        GET_OHIS_DEFAULT_DATA_SUCCESSFUL: getTenantDefaultDataWasSuccessful,
        GET_OHIS_DEFAULT_DATA_UNSUCCESSFUL: getTenantDefaultDataWasUnsuccessful,
        STORE_BENEFICIARY_WAS_SUCCESSFUL: storeBeneficiaryWasSuccessful,
        STORE_ORGANIZATION_WAS_SUCCESSFUL: storeOrganizationWasSuccessful
    }

    return lookup[action.type] ? lookup[action.type](state, action) : state;
}

export default reducer;