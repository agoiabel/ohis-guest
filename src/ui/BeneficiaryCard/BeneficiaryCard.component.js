import React from 'react';
import Card from '../Card';
import CustomButton from '../CustomButton';
import styles from './BeneficiaryCard.component.module.css';

import html2canvas from "html2canvas";
import domtoimage from 'dom-to-image';


class BeneficiaryCard extends React.Component {

	downloadPdf = () => {
		const elementToDownload = document.getElementById("slip");

		let options = {
			allowTaint: false,
			useCORS: true
		};

		html2canvas(elementToDownload, options).then(function (canvas) {
			const link = document.createElement("a");
			const image = canvas.toDataURL("image/png");
			
			link.setAttribute("href", image);
			link.setAttribute("download", "enrollee_slip.png");
			link.click();
		});
	}


	render() {
		return (
			<div className={styles.container}>
				<div id="slip">
					<Card beneficiary={this.props.beneficiary} transaction={this.props.transaction} />
				</div>

				<div className={styles.buttonContainer}>
					<div className={styles.button}>
						<CustomButton onClick={this.downloadPdf}>Download</CustomButton>
					</div>
					<a className={styles.cancelOrBack} onClick={this.props.nextStep}>Enroll Another Beneficiary</a>
				</div>
			</div>
		)
	}
}


export default BeneficiaryCard;