import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import CustomButton from '../../ui/CustomButton';
import styles from './BeneficiaryPlanDetail.component.module.css';

class BeneficiaryPlanDetail extends React.Component {


	componentDidMount() {
		console.dir(this.props);
	};

	componentWillReceiveProps(nextProps) {
		console.dir(nextProps);
	}

	render () {

		let services;

		if (this.props.policy !== null) {
			services = this.props.policy.plan.plan_services.map(planService => (
						<tr key={planService.id}>
							<td>{this.props.policy.plan.name}</td>
							<td>{planService.service.name}</td>
						</tr>
			));
		}

		return (
			<div>
				<div>
					<table className={styles.table}>
						<thead>
							<tr>
								<th>Plan</th>
								<th>Service</th>
							</tr>
						</thead>
						<tbody>

							{ services }

						</tbody>
					</table>
				</div>

				<div className={styles.organizationFooter}>
					<div className={styles.buttonContainer}>
						<div className={styles.button}>
							<CustomButton onClick={this.props.onSubmit}>Start</CustomButton>
						</div>

						<Link to={{ pathname: '/' }} className={styles.cancelOrBack}> Cancel </Link>
					</div>

					<a className={styles.link}>
						{/* Don't want to register as an HMO? Click here to change category */}
					</a>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		// defaultHmo: state.rootReducer.defaultHmo,
	}
}

const mapDispatchToProps = dispatch => {
	return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(BeneficiaryPlanDetail);