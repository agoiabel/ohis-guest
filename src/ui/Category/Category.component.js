import React from 'react';
import { Link } from 'react-router-dom';
import styles from './Category.component.module.css';

const Category = props => {

	const classes = props.showBorder ? [styles.container, styles.addBorder] : [styles.container] 

	return (
		<div className={classes.join(" ")}>
			<div className={styles.title}>{props.name}</div>	
			<div className={styles.subTitle}>Organization</div>
			<div className={styles.description}>
				{props.description}
			</div>

			<div className={styles.getStartedButton}>
				<Link className={styles.getStarted} to={{ pathname: `${props.url}` }}>Get Started</Link>
				<img src={require('../../assets/images/forward-arrow.png')} />
			</div>
		</div>
	);
}

export default Category;