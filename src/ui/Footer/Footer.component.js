import React from 'react';
import styles from './Footer.component.module.css';

const Footer = props => {
	return (
		<footer className={styles.container}>
			
			<div className={styles.contactContainer}>
				<div className={styles.contactInfo}>
					<div className={styles.title}>Contact us</div>

					<div className={styles.info}>
						<div>+234 (0) 803 538 7527</div>
						<div>info@healthspecsltd.com</div>
					</div>

				</div>	
				<div className={styles.copyright}>
					<a href="http://healthspecsltd.com/" target="_blank"><img src={require('../../assets/images/hs_logo.png')} /></a>
				</div>
			</div>			

		</footer>
	);
}

export default Footer;