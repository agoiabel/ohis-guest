import React from 'react';
import { Link } from 'react-router-dom'; 
import styles from './Header.component.module.css';

const Header = props => (
    <div className={styles.headerContainer}>
        <div className={styles.container}>

            <div className={styles.header}>
                <Link className={styles.logoContainer} to={{ pathname: '/' }}>
                    <div className={styles.logoImage}><img src={require('../../assets/images/logo.jpeg')} /></div>
                    <div className={styles.logoName}>OSUN HEALTH SOLUTIONS</div>
                </Link>

                <nav className={styles.navs}>
                    <Link to={{ pathname: '/organization' }} className={styles.navItem}>CATEGORIES</Link>
                    <Link to={{ pathname: '/policy' }} className={styles.navItem}>INSURANCE PACKAGE</Link>
                    <a target="_blank" href="http://healthspecsng.com" className={styles.navItem}>LOGIN</a>
                </nav>
            </div>

        </div>
    </div>
)

export default Header;