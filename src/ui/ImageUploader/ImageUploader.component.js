import React from 'react';
import {connect} from 'react-redux';
import styles from './ImageUploader.component.module.css';

import { openModal, closeModal } from '../Modal/Modal.action';
import TakePictureComponent, { TakePicture } from '../Modal/TakePicture';

class ImageUploader extends React.Component {

	state = {
		imageUri: require('../../assets/images/no-image.png')
	}

	triggerInputFile = () => this.fileInput.click();

	onFileChange = event => {
		const file = event.target.files[0];

		if (event.target.files && file) {
			this.setState({
				imageUri: URL.createObjectURL(file)
			}, () => {
				this.props.fileInputHandler(file);
			});		
		}
	}

	openModal = () => {
		window.scrollTo({
			top: 0,
			left: 0,
			behavior: 'smooth'
		});

		this.props.openModal(TakePicture, {});
	}

	setImage = file => {
		this.props.closeModal();
		this.setState({
			imageUri: URL.createObjectURL(file)
		}, () => {
			this.props.fileInputHandler(file);
		});
	}

	componentDidMount() {}

	componentWillReceiveProps(nextProps) {
		this.setImage(nextProps.file);
	}

	render() {
		return (
			<div className={styles.container}>
				<div className={styles.imageContainer}>
					<img src={this.state.imageUri} />
				</div>
				<div className={styles.uploadPicture}>
					<button className={styles.button} onClick={this.openModal}>
						<img src={require('../../assets/images/camera.png')} />
						<span>Take a picture with camera</span>
					</button>

					<button className={styles.button} onClick={this.triggerInputFile}>
						<img src={require('../../assets/images/upload.png')} />
						<span>Upload from device</span>
						<input type="file" accept="image/*" ref={fileInput => this.fileInput = fileInput} onChange={this.onFileChange} className={styles.upload_image} />
					</button>
				</div>
			</div>		
		)
	}
}


const mapStateToProps = state => {
	return {
		file: state.takePictureReducer.file
	}
}

const mapDispatchToProps = dispatch => {
	return {
		openModal: (modalType, modalProp) => dispatch(openModal(modalType, modalProp)),
		closeModal: () => dispatch( closeModal() ),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ImageUploader);