import React from 'react';
import { connect } from 'react-redux';
import { closeModal } from '../Modal.action';
import { store_picture } from './TakePicture.action';
import styles from './TakePicture.component.module.css';
import CameraPhoto, { FACING_MODES } from 'jslib-html5-camera-photo';

class TakePicture extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.cameraPhoto = null;
        this.videoRef = React.createRef();
        this.state = {
            dataUri: '',
            pictureTaken: false
        }
    }

    componentDidMount() {
        this.cameraPhoto = new CameraPhoto(this.videoRef.current);
    }

    openCamera = () => {
        let facingMode = FACING_MODES.ENVIRONMENT;
        let idealResolution = { width: 640, height: 480 };

        this.cameraPhoto.startCamera(facingMode, idealResolution)
            .then(() => {
                console.log('camera is started !');
            })
            .catch((error) => {
                console.error('Camera not started!', error);
            });
    }

    takePhoto = () => {
        const config = {
            sizeFactor: 1
        };

        let dataUri = this.cameraPhoto.getDataUri(config);

        this.setState({ 
            dataUri: dataUri,
            pictureTaken: true
        });
    }


    dataURItoBlob = (dataURI) => {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], { type: mimeString });
    }

    blobToFile = (theBlob, fileName) => {
        theBlob.lastModifiedDate = new Date();
        theBlob.name = fileName;
        return theBlob;
    }

    finishTaken = () => {
        this.cameraPhoto.stopCamera()
            .then(() => {
                var file = this.blobToFile(this.dataURItoBlob(this.state.dataUri), "picture.png");

                console.dir(file);
                this.props.store_picture(file);
            })
            .catch((error) => {
                console.log('No camera to stop!:', error);
            });
    }

    render() {


        let container = (
            <div>
                <div className={styles.imageContainer}>
                    <video
                        ref={this.videoRef}
                        autoPlay={true}
                    />
                </div>

                <div className={styles.buttons}>
                    <button className={styles.button} onClick={this.openCamera}>Start</button>

                    <button className={styles.button} onClick={this.takePhoto}>Snap</button>
                </div>
            </div>
        );

        if (this.state.pictureTaken) {
            container = (            
                <div>
                    <div className={styles.imageContainer}>
                        <img src={this.state.dataUri} />
                    </div>            

                    <div className={styles.buttons}>
                        <button className={styles.button} onClick={this.finishTaken}>Finish</button>
                    </div>
                </div>
            )
        }

        return (
            <div className={styles.modal}>
                <div className={styles.body}>
                    <div>
                        <div className={styles.action}>
                            <div className={styles.goBack} onClick={this.props.closeModal}>
                                <i className="fa fa-close" aria-hidden="true"></i>  Close
                            </div>
                        </div>

                        { container }
                    </div>
                </div>
            </div>
        );

    }
}


const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => {
    return {
        closeModal: () => dispatch(closeModal()),
        store_picture: payload => dispatch( store_picture(payload) )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TakePicture);