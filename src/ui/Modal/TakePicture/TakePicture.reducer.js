import { STORE_PICTURE } from './index';

const initialState = {
    file: null,
}

const Reducer = (state = initialState, action) => {
    switch (action.type) {
        case STORE_PICTURE:
            return {
                file: action.payload,
            }

        default: return state;
    }
}

export default Reducer;