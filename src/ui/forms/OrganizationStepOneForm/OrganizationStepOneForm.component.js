import React from 'react';
import CustomInput from '../../CustomInput';
import CustomSelect from '../../CustomSelect';
import CustomButton from '../../CustomButton';
import { Field, reduxForm } from 'redux-form';
import styles from './OrganizationStepOneForm.component.module.css';
import { requiredValidator, emailValidator } from '../../../utils/validation';

class OrganizationStepOneForm extends React.Component {

	state = {
		wardens: [],
	}

	localGovtChosen = (event, newValue, previousValue, name) => {
		let local_government = this.props.local_governments.filter(local_government => newValue == local_government.value);
		this.setState({
			wardens: local_government[0].wardens
		});
	}


	render () {

		let local_government = (
			<Field
				name="local_government"
				component={CustomSelect}
				label="Chose Local Govt."
				placeholder="Select an option"
				validate={[requiredValidator]}
				options={this.props.local_governments.map(local_government => {
					return local_government
				})}
				onChange={this.localGovtChosen}
			/>			
		)

		if (this.props.type === 'new-hospital') {
			local_government = (
				<div className={styles.inlineGroup}>
					<div className={styles.formGroup}>
						<Field
							name="local_government"
							component={CustomSelect}
							label="Chose Local Govt."
							placeholder="Select an option"
							validate={[requiredValidator]}
							options={this.props.local_governments.map(local_government => {
								return local_government
							})}
							onChange={this.localGovtChosen}
						/>
					</div>
					<div className={styles.formGroup}>
						<Field
							name="warden"
							component={CustomSelect}
							label="Chose Warden"
							placeholder="Select an option"
							options={this.state.wardens.map(warden => {
								return warden
							})}
							onChange={this.wardenChosen}
						/>
					</div>
				</div>
			)
		}

		return (
			<form onSubmit={this.props.handleSubmit}>

				<div className={styles.inlineGroup}>
					<div className={styles.formGroup}>
						<Field
							name="name"
							component={CustomInput}
							type="text"
							label="Organization name"
							validate={[requiredValidator]}
						/>
					</div>

					<div className={styles.formGroup}>
						<Field
							name="code"
							component={CustomInput}
							type="text"
							label="Organization code"
							validate={[requiredValidator]}
						/>
					</div>
				</div>

				{ local_government }

				<div className={styles.inlineGroup}>
					<div className={styles.formGroup}>
						<Field
							name="organization_address"
							component={CustomInput}
							type="text"
							label="Organization address"
							validate={[requiredValidator]}
						/>
					</div>

					<div className={styles.formGroup}>
						<Field
							name="state"
							component={CustomSelect}
							label="State Organization is Located"
							validate={[requiredValidator]}
							options={[
								{
									displayValue: 'Osun',
									value: 'Osun'
								},
								{
									displayValue: 'Lagos',
									value: 'Lagos',
								},
								{
									displayValue: 'Others',
									value: 'Others',
								},
							]}
						/>
					</div>
				</div>


				<Field
					name="email"
					component={CustomInput}
					type="email"
					label="Your personal email"
					validate={[requiredValidator, emailValidator]}
				/>

				<div className={styles.organizationFooter}>
					<div className={styles.buttonContainer}>
						<div className={styles.button}>
							<CustomButton disabled={this.props.invalid || this.props.pristine}>Next</CustomButton>
						</div>

						<a className={styles.cancelOrBack}>
							Cancel
						</a>
					</div>

					<a className={styles.link}></a>
				</div>

			</form>
		);
	}
}

export default reduxForm({
	form: 'OrganizationStepOneForm'  
})(OrganizationStepOneForm);