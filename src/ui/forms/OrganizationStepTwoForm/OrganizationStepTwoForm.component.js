import React from 'react';
import CustomInput from '../../CustomInput';
import CustomSelect from '../../CustomSelect';
import CustomButton from '../../CustomButton';
import { Field, reduxForm } from 'redux-form';
import { requiredValidator } from '../../../utils/validation';
import styles from './OrganizationStepTwoForm.component.module.css';

const OrganizationStepOneForm = props => {

	const { handleSubmit, pristine, invalid, submittingForm } = props

	return (
		<form onSubmit={handleSubmit}>


			<Field
				name="bank_name"
				component={CustomSelect}
				label="Organization bank name"
				validate={[requiredValidator]}
				options={[
					{
						displayValue: 'GTB',
						value: 'GTB'
					},
					{
						displayValue: 'UNION BANK',
						value: 'UNION BANK'
					},
				]}
			/>


			<div className={styles.inlineGroup}>
				
				<div className={styles.formGroup}>
					<Field
						name="account_name"
						component={CustomInput}
						type="text"
						label="Please what is the account name"
						validate={[requiredValidator]}
					/>
				</div>

				<div className={styles.formGroup}>
					<Field
						name="account_number"
						component={CustomInput}
						type="number"
						label="Please what is the account number"
						validate={[requiredValidator]}
					/>
				</div>

			</div>



			<div className={styles.footer}>

				<div className={styles.buttonContainer}>
					<div className={styles.button}>
						<CustomButton disabled={invalid || pristine} submittingForm={submittingForm}>Next</CustomButton>
					</div>

					<a className={styles.cancelOrBack} onClick={props.previousStep}>
						Previous
					</a>
				</div>

			</div>

		</form>
	);
}

export default reduxForm({
	form: 'OrganizationStepOneForm'  
})(OrganizationStepOneForm);