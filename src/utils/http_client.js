import { getStorage } from './storage.js';

const url = 'http://api.healthspecsng.com/api/';
// const url = 'http://new-health-connect-api.test/api/';


export const post = async (formData, end_point) => {

    return fetch(url + end_point, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            "AuthorizationHeader": "Bearer OSUN_1234567890987654321"
        },
        body: JSON.stringify(formData)
    });

}


export const get = async end_point => {

    return fetch(url + end_point, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            "AuthorizationHeader": "Bearer OSUN_1234567890987654321"
        },
    });

}


export const postWithImage = async (image_name, formData, end_point) => {

    let allFormData = new FormData();
    allFormData.append(image_name, formData.picture);
    allFormData.append('data', JSON.stringify(formData.data));

    return fetch(url + end_point, {
        method: 'POST',
        body: allFormData,
        headers: {
            "AuthorizationHeader": "Bearer OSUN_1234567890987654321"
        }
    });

}